package ma.octo.assignement.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import ma.octo.assignement.domain.util.EventType;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "OPERATION")
@Getter
@Setter
public class Operation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(precision = 16, scale = 2, nullable = false)
    private BigDecimal amount;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateExecution;

    @Column(length = 200)
    private String motif;

    @Enumerated(EnumType.STRING)
    private EventType eventType;

    @Column
    private String fullNameSender;

    @ManyToOne
    @JoinColumn(name = "sender_id")
    @JsonManagedReference("sender")
    private Account accountSender;

    @ManyToOne
    @JoinColumn(name = "receiver_id")
    @JsonManagedReference("receiver")
    private Account accountReceiver;
    @ManyToOne
    private Audit audit;
}
