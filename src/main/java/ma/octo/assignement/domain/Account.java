package ma.octo.assignement.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "T_ACCOUNT")
@Getter
@Setter
@DynamicUpdate
public class Account implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(length = 16, unique = true)
  private String nrCompte;

  private String rib;


  @Column(precision = 16, scale = 2)
  private BigDecimal balance;

  @ManyToOne()
  @JoinColumn(name = "customer_id")
  private Customer customer;

  @OneToMany(mappedBy = "accountSender", cascade = CascadeType.ALL)
  private List<Operation> operations;

  //operations

}
