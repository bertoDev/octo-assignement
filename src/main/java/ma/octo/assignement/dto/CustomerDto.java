package ma.octo.assignement.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
public class CustomerDto implements Serializable {
    private  Long id;
    private  String username;
    private  String gender;
    private  String lastname;
    private  String firstname;
    private  Date birthdate;
    private Set<AccountDto> bankAccounts;
}
