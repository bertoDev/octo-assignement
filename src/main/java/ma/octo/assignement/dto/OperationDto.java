package ma.octo.assignement.dto;

import lombok.Getter;
import lombok.Setter;
import ma.octo.assignement.domain.util.EventType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class OperationDto implements Serializable {
    private  Long id;
    private  BigDecimal amount;
    private  Date dateExecution;
    private  String motif;
    private  EventType eventType;
    private  String fullNameSender;
    private  AccountDto accountSender;
    private  AccountDto accountReceiver;
    private  AuditDto audit;
}
