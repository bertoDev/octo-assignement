package ma.octo.assignement.dto;

import lombok.Getter;
import lombok.Setter;
import ma.octo.assignement.domain.util.EventType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class AuditDto implements Serializable {
    private  Long id;
    private  String message;
    private  EventType eventType;
}
