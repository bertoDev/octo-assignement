package ma.octo.assignement.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@DynamicUpdate
public class TransferRequestDto {
  private String nrSenderAccount;
  private String nrReceiverAccount;
  private String motif;
  private BigDecimal amount;
  private Date date;
}
