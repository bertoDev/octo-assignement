package ma.octo.assignement.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
public class AccountDto implements Serializable {
    private  Long id;
    private  String nrCompte;
    private  String rib;
    private  BigDecimal balance;
    private  CustomerDto customer;
}
