package ma.octo.assignement.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
@Getter
@Setter
@DynamicUpdate
public class DepositRequestDto implements Serializable {
private String fullNameSender;
private String ribAccountReceiver;
private String motif;
private BigDecimal amount;
private Date date;
}
