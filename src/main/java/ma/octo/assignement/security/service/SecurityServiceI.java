package ma.octo.assignement.security.service;

import ma.octo.assignement.security.entities.AppRole;
import ma.octo.assignement.security.entities.AppUser;

import java.util.List;

public interface SecurityServiceI {
    AppUser saveNewUser(AppUser user);
    AppRole saveNewRole(AppRole role);
    void addRoleToUser(String username,String rolename);

    AppUser loadUserByUsername(String username);
    void removeRoleToUser(String username,String rolename);

    List<AppUser> listUsers();
}
