package ma.octo.assignement.security.entities;

import lombok.Data;

@Data

public class RoleUserForm {
    private String username;
    private String roleName;
}
