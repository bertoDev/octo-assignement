package ma.octo.assignement;

import ma.octo.assignement.dto.AccountDto;
import ma.octo.assignement.dto.CustomerDto;
import ma.octo.assignement.dto.TransferRequestDto;
import ma.octo.assignement.security.entities.AppRole;
import ma.octo.assignement.security.entities.AppUser;
import ma.octo.assignement.security.service.SecurityServiceI;
import ma.octo.assignement.service.AccountService;
import ma.octo.assignement.service.CustomerService;
import ma.octo.assignement.service.OperationService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.math.BigDecimal;

@SpringBootApplication
@EnableWebSecurity
public class NiceBankApplication {



	public static void main(String[] args) {
		SpringApplication.run(NiceBankApplication.class, args);
	}
	@Bean
	PasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder();
	}
@Bean
	CommandLineRunner commandLineRunner(AccountService accountService, CustomerService customerService,
										OperationService operationService, SecurityServiceI securityServiceI){
		return args ->{
			CustomerDto customer1 = new CustomerDto();
			customer1.setUsername("customer1");
			customer1.setLastname("last1");
			customer1.setFirstname("first1");
			customer1.setGender("Male");

			CustomerDto saveCustomer1= customerService.createCustomer(customer1);


			CustomerDto customer2 = new CustomerDto();
			customer2.setUsername("customer2");
			customer2.setLastname("last2");
			customer2.setFirstname("first2");
			customer2.setGender("Female");

			CustomerDto saveCustomer2 =customerService.createCustomer(customer2);

			AccountDto account1 = new AccountDto();
			account1.setNrCompte("010000A000001000");
			account1.setRib("RIB1");
			account1.setBalance(BigDecimal.valueOf(200000L));
			account1.setCustomer(saveCustomer1);

			accountService.createAccount(account1);

			AccountDto account2 = new AccountDto();
			account2.setNrCompte("010000B025001000");
			account2.setRib("RIB2");
			account2.setBalance(BigDecimal.valueOf(140000L));
			account2.setCustomer(saveCustomer2);

			accountService.createAccount(account2);

			TransferRequestDto tranfer = new TransferRequestDto();
			tranfer.setAmount(BigDecimal.valueOf(10000L));
			tranfer.setNrReceiverAccount(account2.getNrCompte());
			tranfer.setNrSenderAccount(account1.getNrCompte());
			tranfer.setMotif("Assignment 2021");
			operationService.createTranfer(tranfer);

			AppUser appUser=new AppUser();
			appUser.setUsername("Bertrand");appUser.setPassword("1234"); appUser.setActive(true);
			securityServiceI.saveNewUser(appUser);
			AppUser appUser1=new AppUser();appUser1.setActive(true);
			appUser1.setUsername("Mohammed");appUser1.setPassword("1234");
			securityServiceI.saveNewUser(appUser1);
			AppUser appUser2=new AppUser();appUser2.setActive(true);
			appUser2.setUsername("Yasmine");appUser2.setPassword("1234");
			securityServiceI.saveNewUser(appUser2);

			AppRole appRole=new AppRole();
			appRole.setRolename("USER");
			securityServiceI.saveNewRole(appRole);
			AppRole appRole1=new AppRole();
			appRole1.setRolename("ADMIN");
		 	securityServiceI.saveNewRole(appRole1);

			securityServiceI.addRoleToUser("Bertrand", "USER");
			securityServiceI.addRoleToUser("Bertrand", "ADMIN");
			securityServiceI.addRoleToUser("Mohammed", "USER");
			securityServiceI.addRoleToUser("Yasmine", "USER");
		};

}
}
