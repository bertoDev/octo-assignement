package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Operation;
import ma.octo.assignement.domain.util.EventType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface OperationRepository extends JpaRepository<Operation, Long> {
    List<Operation> findOperationByEventType(EventType eventType);

}

