package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
  Account findByNrCompte(String nrCompte);
  Optional <Account> findAccountByRib(String rib);
}
