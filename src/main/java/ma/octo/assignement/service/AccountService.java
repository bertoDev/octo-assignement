package ma.octo.assignement.service;

import ma.octo.assignement.dto.AccountDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import org.springframework.data.domain.Page;

import java.util.List;

public interface AccountService {
    List<AccountDto> getALLAccount();
    AccountDto getAccountById(Long id) throws Exception;
    AccountDto createAccount(AccountDto accountDto);
    AccountDto updateAccount(AccountDto accountDto, Long id);
    void deleteAccount(Long id);
    Page<AccountDto> getAccountsByPage(int page, int size);

}
