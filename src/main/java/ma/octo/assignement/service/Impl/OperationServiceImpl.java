package ma.octo.assignement.service.Impl;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.Operation;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.AuditDto;
import ma.octo.assignement.dto.DepositRequestDto;
import ma.octo.assignement.dto.OperationDto;
import ma.octo.assignement.dto.TransferRequestDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.ResourceNotFoundException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.OperationMapper;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.AuditRepository;
import ma.octo.assignement.repository.OperationRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.OperationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class OperationServiceImpl implements OperationService {
    private  OperationRepository operationRepository;

    private  AuditRepository auditRepository;
    private  AccountRepository accountRepository;
    private  OperationMapper operationMapper;

    @Override
    public List<OperationDto> getAllOperations() {
        List<Operation> operations= operationRepository.findAll();
        return operations
                .stream()
                .map(operationMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<OperationDto> getAllTransferOperations() {
        List<Operation> operations= operationRepository.findOperationByEventType(EventType.TRANSFER);
        return operations
                .stream()
                .map(operationMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<OperationDto> getAllDepositOperations() {
        List<Operation> operations= operationRepository.findOperationByEventType(EventType.DEPOSIT);
        return operations
                .stream()
                .map(operationMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public OperationDto getOperationById(Long id) throws ResourceNotFoundException {
        Optional<Operation> operation = operationRepository.findById(id);
        if (!operation.isPresent()){
            throw new ResourceNotFoundException("Operation with ID " + id + "Not Found");
        }
        return operationMapper.toDto(operation.get());
    }

    @Override
    public List<OperationDto> getOperationsByPage(int page, int size) {
        return null;
    }

    @Override
    public List<OperationDto> getOperationsByType(String type) {
        return null;
    }

    @Override
    public OperationDto deposit(DepositRequestDto depositRequestDto) throws Exception {
        validate_deposit(depositRequestDto);
        Optional <Account> account = accountRepository.findAccountByRib(depositRequestDto.getRibAccountReceiver());

        //treatement
        account.get().setBalance(account.get().getBalance().add(depositRequestDto.getAmount()));
        accountRepository.save(account.get());

        //save operation
        Operation operation=new Operation();
        operation.setAmount(depositRequestDto.getAmount());
        operation.setAccountReceiver(account.get());
        operation.setFullNameSender(depositRequestDto.getFullNameSender());
        operation.setEventType(EventType.DEPOSIT);
        operation.setDateExecution(new Date());

        Operation saveOperation= operationRepository.save(operation);

        //save audit
        Audit audit=new Audit();
        audit.setEventType(EventType.DEPOSIT);
        audit.setMessage("Deposit of "+depositRequestDto.getAmount()+" to "+account.get().getRib());
        auditRepository.save(audit);

        return operationMapper.toDto(saveOperation);
    }

    @Override
    public OperationDto createTranfer(TransferRequestDto transferDto) throws Exception {
        validate_tranfer(transferDto);
        Account senderAccount = accountRepository.findByNrCompte(transferDto.getNrSenderAccount());
        Account receiverAccount = accountRepository.findByNrCompte(transferDto.getNrReceiverAccount());


        senderAccount.setBalance(senderAccount.getBalance().subtract(transferDto.getAmount()));
        receiverAccount.setBalance(receiverAccount.getBalance().add(transferDto.getAmount()));
        accountRepository.save(senderAccount);
        accountRepository.save(receiverAccount);


        //save operation
        Operation operation=new Operation();
        operation.setAmount(transferDto.getAmount());
        operation.setAccountSender(senderAccount);
        operation.setAccountReceiver(receiverAccount);
        operation.setEventType(EventType.TRANSFER);
        operation.setDateExecution(new Date());
        operation.setMotif(transferDto.getMotif());
        Operation saveOperation= operationRepository.save(operation);

        //audit
        Audit audit=new Audit();
        audit.setEventType(EventType.TRANSFER);
        audit.setMessage("Transfer from "+ senderAccount.getNrCompte()+" to "+ receiverAccount.getNrCompte());
        auditRepository.save(audit);

        return operationMapper.toDto(saveOperation);
    }

    void validate_tranfer(TransferRequestDto transferDto) throws Exception {
        Account senderAccount = accountRepository.findByNrCompte(transferDto.getNrSenderAccount());
        Account receiverAccount = accountRepository.findByNrCompte(transferDto.getNrReceiverAccount());
        if (senderAccount == null) {
            throw new CompteNonExistantException("Account with Number " + transferDto.getNrSenderAccount() + "Not Found");
        }
        if (receiverAccount == null) {
            throw new CompteNonExistantException("Account with Number " + transferDto.getNrReceiverAccount() + "Not Found");
        }
        //veirfy amount
        if(transferDto.getAmount().equals(null)){
            throw new TransactionException("Amount is null");
        }else if(transferDto.getAmount().intValue()<10) {
            throw new TransactionException("Amount is less than Minimum");
        }else if(transferDto.getAmount().intValue()> MONTANT_MAXIMAL) {
            throw new TransactionException("Amount is greater than Maximum");
        }

        //verify balance
        if (senderAccount.getBalance().compareTo(transferDto.getAmount().abs()) < 0) {
            throw new TransactionException("Insufficient Balance");
        }
        // verify motis
        if (transferDto.getMotif().length() < 0) {
            throw new TransactionException("Motif is empty");
        }
    }

    void validate_deposit(DepositRequestDto depositRequestDto) throws Exception {
        // verify account exist
        Optional <Account> account = accountRepository.findAccountByRib(depositRequestDto.getRibAccountReceiver());
        if(!account.isPresent()){
            throw new CompteNonExistantException("Account with RIB "+ depositRequestDto.getRibAccountReceiver() + "Not Found");
        }
        //veirfy amount
        if(depositRequestDto.getAmount().equals(null)){
            throw new TransactionException("Amount is null");
        }else if(depositRequestDto.getAmount().intValue()<10) {
            throw new TransactionException("Amount is less than Minimum");
        }else if(depositRequestDto.getAmount().intValue()> MONTANT_MAXIMAL) {
            throw new TransactionException("Amount is greater than Maximum");
        }

    }


}
