package ma.octo.assignement.service.Impl;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Account;
import ma.octo.assignement.dto.AccountDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.mapper.AccountMapper;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.service.AccountService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {
   private  AccountRepository accountRepository;
   private  AccountMapper accountMapper;

   public AccountServiceImpl(AccountRepository accountRepository, AccountMapper accountMapper) {
       this.accountRepository = accountRepository;
         this.accountMapper = accountMapper;
    }

    @Override
    public List<AccountDto> getALLAccount() {
        List<Account> accounts = accountRepository.findAll();
        return accounts.stream()
                .map(accountMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public AccountDto getAccountById(Long id) throws Exception {
        Optional<Account> account = accountRepository.findById(id);
        if(!account.isPresent()){
            throw new CompteNonExistantException("Compte with ID: "+id+" not found");
        }
        return accountMapper.toDto(account.get());
    }

    @Override
    public AccountDto createAccount(AccountDto accountDto) {
        Account account = accountMapper.toModel(accountDto);
        accountRepository.save(account);
        return accountMapper.toDto(account);
    }

    @Override
    public AccountDto updateAccount(AccountDto accountDto, Long id) {
        return null;
    }

    @Override
    public void deleteAccount(Long id) {

    }

    @Override
    public Page<AccountDto> getAccountsByPage(int page, int size) {
        Page<Account> accounts = accountRepository.findAll(PageRequest.of(page, size));
       Page<AccountDto> accountDtos = accounts.map(accountMapper::toDto);
        return accountDtos;
    }
}
