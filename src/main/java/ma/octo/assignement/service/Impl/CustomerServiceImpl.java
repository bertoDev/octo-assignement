package ma.octo.assignement.service.Impl;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Customer;
import ma.octo.assignement.dto.CustomerDto;
import ma.octo.assignement.mapper.CustomerMapper;
import ma.octo.assignement.repository.CustomerRepository;
import ma.octo.assignement.service.CustomerService;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional
public class CustomerServiceImpl implements CustomerService {
    private  CustomerRepository customerRepository;
    private  CustomerMapper customerMapper;

    @Override
    public List<CustomerDto> getAllCustomers() {
        List<Customer> customers=customerRepository.findAll();
        if(customers.size()<1) return null;
        return customers.stream()
                .map(customerMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public CustomerDto getCustomerById(Long id) {
        Customer customer=customerRepository.findById(id).get();
        return customerMapper.toDto(customer);
    }

    @Override
    public CustomerDto createCustomer(CustomerDto customerDto) {
        Customer customer=customerMapper.toModel(customerDto);
        Customer saveCustomer= customerRepository.save(customer);
        return customerMapper.toDto(saveCustomer);
    }

    @Override
    public CustomerDto updateCustomer(CustomerDto customerDto, Long id) {
        return null;
    }

    @Override
    public void deleteCustomer(Long id) {

    }

    @Override
    public List<CustomerDto> getCustomersByPage(int page, int size) {
        return null;
    }
}
