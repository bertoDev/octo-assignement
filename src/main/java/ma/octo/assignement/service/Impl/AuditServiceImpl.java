package ma.octo.assignement.service.Impl;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.dto.AuditDto;
import ma.octo.assignement.mapper.AuditMapper;
import ma.octo.assignement.repository.AuditRepository;
import ma.octo.assignement.service.AuditService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional

public class AuditServiceImpl implements AuditService {
    private  AuditRepository auditRepository;
    private  AuditMapper auditMapper;
    public AuditServiceImpl(AuditRepository auditRepository, AuditMapper auditMapper) {
        this.auditRepository = auditRepository;
        this.auditMapper = auditMapper;
    }

    Logger LOGGER= LoggerFactory.getLogger(AuditServiceImpl.class);

    // add  tranfer and depot
    @Override
    public AuditDto  createAudit(AuditDto auditDto) {
        LOGGER.info("Audit de l'événement {}", auditDto.getEventType());
        Audit audit= auditMapper.toModel(auditDto);
        Audit saveAudit= auditRepository.save(audit);
        return auditMapper.toDto(saveAudit);
    }

    @Override
    public List<AuditDto> getAllByPage(int page, int size) {
        return null;
    }

    @Override
    public List<AuditDto> getAllAudits() {
        List<Audit> audits= auditRepository.findAll();
        return audits
                .stream()
                .map(auditMapper::toDto)
                .collect(Collectors.toList());
    }

}
