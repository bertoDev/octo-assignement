package ma.octo.assignement.service;

import ma.octo.assignement.dto.CustomerDto;

import java.util.List;

public interface CustomerService {
    List<CustomerDto> getAllCustomers();
    CustomerDto getCustomerById(Long id);
    CustomerDto createCustomer(CustomerDto customerDto);
    CustomerDto updateCustomer(CustomerDto customerDto, Long id);
    void deleteCustomer(Long id);
    List<CustomerDto> getCustomersByPage(int page, int size);

}
