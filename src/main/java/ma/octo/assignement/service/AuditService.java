package ma.octo.assignement.service;

import ma.octo.assignement.dto.AuditDto;

import java.util.List;

public interface AuditService {
    public AuditDto  createAudit(AuditDto auditDto);
    List<AuditDto>  getAllByPage(int page, int size);
    List<AuditDto>  getAllAudits();

}
