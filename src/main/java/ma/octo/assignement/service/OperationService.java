package ma.octo.assignement.service;

import ma.octo.assignement.dto.DepositRequestDto;
import ma.octo.assignement.dto.OperationDto;
import ma.octo.assignement.dto.TransferRequestDto;
import ma.octo.assignement.exceptions.ResourceNotFoundException;

import java.util.List;

public interface OperationService {
    public static final int MONTANT_MAXIMAL = 10000;
    List<OperationDto> getAllOperations();
    List<OperationDto> getAllTransferOperations();
    List<OperationDto> getAllDepositOperations();
    OperationDto getOperationById(Long id) throws ResourceNotFoundException;
    List<OperationDto> getOperationsByPage(int page, int size);

    List<OperationDto> getOperationsByType(String type);

    //depot or tanfer
    OperationDto deposit(DepositRequestDto depositRequestDto) throws Exception;

    OperationDto createTranfer(TransferRequestDto tranferDto) throws Exception;


}
