package ma.octo.assignement.exceptions;

public class MaxAmountException extends Exception{
    private static final long serialVersionUID = 1L;

    public MaxAmountException(){}
    public MaxAmountException(String message){
        super(message);
    }

}
