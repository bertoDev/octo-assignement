package ma.octo.assignement.web;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Operation;
import ma.octo.assignement.dto.DepositRequestDto;
import ma.octo.assignement.dto.OperationDto;
import ma.octo.assignement.dto.TransferRequestDto;
import ma.octo.assignement.exceptions.ResourceNotFoundException;
import ma.octo.assignement.service.OperationService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/operations")
@AllArgsConstructor
public class OperationController {
    private OperationService operationService;

    @GetMapping
    List<OperationDto> getAllOperations() {
        return operationService.getAllOperations();
    }

    @GetMapping("/{id}")
    OperationDto getOperationById(@PathVariable Long id) throws ResourceNotFoundException {
        return operationService.getOperationById(id);
    }

    @PostMapping("/deposit")
    OperationDto deposit( @RequestBody DepositRequestDto operationDto) throws Exception {
        return operationService.deposit(operationDto);
    }

    @PostMapping("/transfer")
    OperationDto createTranfer( @RequestBody TransferRequestDto tranferDto) throws Exception {
        return operationService.createTranfer(tranferDto);
    }

}
