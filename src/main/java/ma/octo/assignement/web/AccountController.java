package ma.octo.assignement.web;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import ma.octo.assignement.dto.AccountDto;
import ma.octo.assignement.service.AccountService;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/accounts")
@AllArgsConstructor
public class AccountController {
    private  AccountService accountService;

    @GetMapping
    List<AccountDto> getAllAccounts() {
        return accountService.getALLAccount();
    }
    @GetMapping("/page")
    Page<AccountDto> getAccountsByPage(int page, int size) {
        return accountService.getAccountsByPage(page, size);
    }

    @PostMapping
    AccountDto createAccount( @RequestBody AccountDto accountDto) {
        return accountService.createAccount(accountDto);
    }

    @GetMapping("/{id}")
    AccountDto getAccountById(@PathVariable Long id) throws Exception {
        return accountService.getAccountById(id);
    }


}
