package ma.octo.assignement.web;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import ma.octo.assignement.dto.CustomerDto;
import ma.octo.assignement.service.CustomerService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customers")
@AllArgsConstructor
public class CustomerController {
    private  CustomerService customerService;

    @GetMapping()
    List<CustomerDto> getAllCustomers() {
        return customerService.getAllCustomers();
    }

    @PostMapping
    CustomerDto createCustomer( @RequestBody CustomerDto customerDto) {
        return customerService.createCustomer(customerDto);
    }

    @GetMapping("/{id}")
    CustomerDto getCustomerById(@PathVariable Long id) {
        return customerService.getCustomerById(id);
    }

}
