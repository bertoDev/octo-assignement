package ma.octo.assignement.web;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ma.octo.assignement.dto.AuditDto;
import ma.octo.assignement.service.AuditService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/audits")
@AllArgsConstructor
public class AuditController {
    private  AuditService auditService;

    @GetMapping
    List<AuditDto> getAllAudits() {
        return auditService.getAllAudits();
    }
    @PostMapping
    AuditDto createAudit(AuditDto auditDto) {
        return auditService. createAudit(auditDto);
    }
}
