package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Operation;
import ma.octo.assignement.dto.OperationDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring",uses = { AccountMapper.class, AuditMapper.class})
public interface OperationMapper {
    OperationDto toDto(Operation operationDto);
    Operation toModel(OperationDto operationDto);

}
