package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.dto.AuditDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AuditMapper {
    AuditDto toDto(Audit audit);
    Audit toModel(AuditDto auditDto);

}
