package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Customer;
import ma.octo.assignement.dto.CustomerDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerMapper {
    CustomerDto toDto(Customer customer);
    Customer toModel(CustomerDto customerDto);
}
