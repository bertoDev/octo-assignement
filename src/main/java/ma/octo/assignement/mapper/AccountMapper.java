package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.dto.AccountDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {OperationMapper.class, CustomerMapper.class})
public interface AccountMapper {
    AccountDto toDto(Account account);
    Account toModel(AccountDto accountDto);
}
