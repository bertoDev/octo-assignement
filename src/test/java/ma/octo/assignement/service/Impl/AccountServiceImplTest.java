package ma.octo.assignement.service.Impl;

import ma.octo.assignement.dto.AccountDto;
import ma.octo.assignement.dto.CustomerDto;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.OperationRepository;
import ma.octo.assignement.service.AccountService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class AccountServiceImplTest {

    @Mock
    private AccountRepository accountRepository;
    @Autowired
    private AccountService underTest;

    @BeforeAll
    void setUp() {
        MockitoAnnotations.openMocks(this);
        CustomerDto customer1 = new CustomerDto();
        customer1.setUsername("customer1");
        customer1.setLastname("last1");
        customer1.setFirstname("first1");
        customer1.setGender("Male");

        CustomerDto customer2 = new CustomerDto();
        customer2.setUsername("customer2");
        customer2.setLastname("last2");
        customer2.setFirstname("first2");
        customer2.setGender("Female");


        AccountDto account1 = new AccountDto();
        account1.setNrCompte("010000A000001000");
        account1.setRib("RIB1");
        account1.setBalance(BigDecimal.valueOf(200000L));
        account1.setCustomer(customer1);

        AccountDto account2 = new AccountDto();
        account2.setNrCompte("010000B025001000");
        account2.setRib("RIB2");
        account2.setBalance(BigDecimal.valueOf(140000L));
        account2.setCustomer(customer2);

        List<AccountDto> accountDtos = List.of(account1, account2);

    }

    @Test
    void getALLAccount() {
        //given

        //when
        List<AccountDto> accountDtos = underTest.getALLAccount();

        //then
    }

    @Test
    void getAccountById() {
    }

    @Test
    void createAccount() {
    }
}
