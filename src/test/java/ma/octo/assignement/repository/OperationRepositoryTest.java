package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Operation;
import ma.octo.assignement.domain.util.EventType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class OperationRepositoryTest {
    @Autowired
    private OperationRepository underTest;

    @Test
    void findOperationByEventType() {
        Operation operation = new Operation();
        operation.setEventType(EventType.DEPOSIT);
        operation.setAmount(new BigDecimal(300));
        underTest.save(operation);

        //When
        Operation foundOperation = underTest.findOperationByEventType(EventType.DEPOSIT).get(0);

        //Then
        assertEquals(foundOperation.getEventType(), operation.getEventType());
    }


}
