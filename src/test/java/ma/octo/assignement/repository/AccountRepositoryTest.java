package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Account;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class AccountRepositoryTest {
    @Autowired
    private AccountRepository underTest;

    @Test
    void findByNrCompte() {
        Account account = new Account();
        account.setNrCompte("123456789");
        account.setRib("RIB");
        account.setBalance(new BigDecimal(300));
        underTest.save(account);

        //When
        Account foundAccount = underTest.findByNrCompte("123456789");

        //Then
        assertEquals(foundAccount.getNrCompte(), account.getNrCompte());

    }

    @Test
    void findAccountByRib() {
    }
}
