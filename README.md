## Description :

Un transfer est un transfert d'argent d'un compte emetteur vers un compte bénéficiaire ...

**Besoin métier :** 

Ajouter un nouveau cas d'usage appelé "Deposit". Le Deposit est un dépôt d'agent cash sur un compte donné (Versement d'argent). 

Imaginez que vous allez à une agence avec un montant de 1000DH et que vous transferez ça en spécifiant le RIB souhaité.
 
L'identifiant fonctionnel d'un compte dans ce cas préçis est le RIB.  


## Assignement :

* Le code présente des anomalies de qualité (bonnes pratiques , abstraction , lisibilité ...) et des bugs . 
    * localiser le maximum 
  
    * Essayer d'améliorer la qualité du code.    
  
    * Essayer de résoudre les bugs détectés. 
* Implementer le cas d'usage `Deposit` 
  Règles de gestion : 
  - Le montant maximal que je peux déposer par opération est 10000DH

* Ajouter des tests unitaires.  

* **Nice to have** : Ajouter une couche de sécurité 

## How to use 
To build the projet you will need : 
* Java 17
* Maven

Build command : 
```
mvn clean install
```

Run command : 
```
./mvnw spring-boot:run 
## or use any prefered method (IDE , java -jar , docker .....)
```

## How to submit 
* Fork the project into your personal gitlab space .    
* Do the stuff.
* Send us the link.



                          BertoDev Refactory Notes:
<br>

## Modelisations

I purpose 4 class to modelise the problem

* Account [👉] (./src/main/java/ma/octo/assignement/domain/BankAccount.java)

* Customer [👉] (./src/main/java/ma/octo/assignement/domain/Customer.java)

* Audit [👉] (./src/main/java/ma/octo/assignement/domain/Audit.java)

* Operation [👉]  (./src/main/java/ma/octo/assignement/domain/Operation.java)

## add DTOs to the project




## add mapper to the project with MapStruct

* add dependency to pom.xml [👉] (./pom.xml)
*  implement the mapper [👉] (./src/main/java/ma/octo/assignement/mapper)

## add service layer to the project
  here I adopt the weak coupling strategy between the controller and the service layer

* add services interface [👉] (./src/main/java/ma/octo/assignement/service)
* add services implementation [👉] (./src/main/java/ma/octo/assignement/service/impl)


## add controller layer to the project

* add controller [👉] (./src/main/java/ma/octo/assignement/controller)


## add Swagger to the project to test the API
 * add dependency to pom.xml [👉] (./pom.xml)
<br>

   ![img_1.png](img_1.png)

<br>


## add security layer to the project

* add security module [👉] (./src/main/java/ma/octo/assignement/security)

* add security entities [👉] (./src/main/java/ma/octo/assignement/security/entities)

* add security config [👉] (./src/main/java/ma/octo/assignement/config/SecurityConfig.java)

* add security controller [👉] (./src/main/java/ma/octo/assignement/controller/SecurityController.java)

* add security service [👉] (./src/main/java/ma/octo/assignement/service/SecurityService.java)

* add security service implementation [👉] (./src/main/java/ma/octo/assignement/service/impl/SecurityServiceImpl.java)

* add security dto [👉] (./src/main/java/ma/octo/assignement/dto/SecurityDto.java)


## unit test Implementation

I try, but I found error with dependancy and injection  didn't have time to finish the unit test implementation

